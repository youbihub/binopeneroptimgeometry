# BinOpenerOptimGeometry

Constraints of problem:

- l0 + l1 = y3 + y4 + k0
- beta = x4/2
- l0 < PJ
- d(Q,PB) > k1 {'in' case}
- d(Q,BC) > k2 {'in' case}
- d(G,PQ) > k3 {'out' case}
- d(I,PQ) > k4 {'out' case}
- {'in' case}: R=I+[ beta,k5]
- add more constraints if needed.

outputs:
-   alpha, beta, l0, l1