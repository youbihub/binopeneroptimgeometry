#pragma once
#include "Eigen/Core"
#include "Eigen/Dense"

namespace smartbin {
  namespace conf {
    namespace data {
      constexpr auto x0{624.};
      constexpr auto x1{300.};
      constexpr auto x2{153.};
      constexpr auto y0{195.};
      constexpr auto y1{612.};
      constexpr auto y2{245.};
      constexpr auto x3{58.};
      constexpr auto y3{330.};
      constexpr auto x4{305.};
      constexpr auto y4{478.};
      constexpr auto rp{20.};
      constexpr auto rq{20.};
      constexpr auto e{20.};
    }  // namespace data
    namespace point {
      const auto A{Eigen::Vector2d{0., 0.}};
      const auto B{(A + Eigen::Vector2d{data::x0, 0.}).eval()};
      const auto C{(B + Eigen::Vector2d{0, -data::y0}).eval()};
      const auto D{(C + Eigen::Vector2d{data::x1, 0}).eval()};
      const auto E{(D + Eigen::Vector2d{0, -data::y1}).eval()};
      const auto F{(E + Eigen::Vector2d{-data::x1 - data::x0 + data::x2, 0}).eval()};
      const auto G{(F + Eigen::Vector2d{0, data::y1 + data::y0 - data::y2}).eval()};
      const auto H{(G + Eigen::Vector2d{-data::x2, 0}).eval()};
      const auto I{(Eigen::Vector2d{data::x2 + data::x3, -data::y3}).eval()};
      const auto J{(I + Eigen::Vector2d{data::x4, 0}).eval()};
      const auto K{(J + Eigen::Vector2d{0, -data::y4}).eval()};
      const auto L{(K + Eigen::Vector2d{-data::x4, 0}).eval()};

    }  // namespace point
  }    // namespace conf
  template <typename D0, typename D1, typename D2, typename D3>
  auto Intersect(const Eigen::MatrixBase<D0>& A, const Eigen::MatrixBase<D1>& B,
                 const Eigen::MatrixBase<D2>& X, const Eigen::MatrixBase<D3>& Y) {
    using T = typename D0::Scalar;
    auto AB{(B - A).eval()};
    auto XY{(Y - X).eval()};
    // A + AB* t(0) = X + XY*t(1);
    // AB* t(0) - XY* t(1) = X - A;
    // [AB; -XY]* t = X - A;
    // t = [AB; -XY].inverse()*(X-A);
    auto M{Eigen::Matrix<T, 2, 2>{}};
    // todo: insert vector
    // clang-format off
    M << AB(0), -(T)XY(0),
         AB(1), -(T)XY(1);
    // clang-format on
    auto t{(M.inverse() * (X - A)).eval()};
    return t(0) >= 0. && t(0) <= 1. && t(1) >= 0. && t(1) <= 1.;
  }
  template <typename D0, typename D1, typename D2> auto Distance(const Eigen::MatrixBase<D0>& A,
                                                                 const Eigen::MatrixBase<D1>& B,
                                                                 const Eigen::MatrixBase<D2>& p) {
    auto P{Eigen::Matrix<typename D0::Scalar, 2, 1>(p(0), p(1))};
    auto BP{P - B};
    auto AB{B - A};

    struct DistSeg {
      typename D0::Scalar d;
      bool inside;
    };

    if (BP.dot(AB) < (typename D0::Scalar)0.) {
      // inside
      auto ABu{AB.normalized()};
      auto M{Eigen::Matrix<typename D0::Scalar, 2, 2>{}};
      M << ABu, BP;
      return DistSeg{M.determinant(), true};
    } else {
      auto AP{P - A};
      auto mm = std::min(AP.norm(), BP.norm());
      return DistSeg{mm, false};
      // return DistSeg{AP.norm() > BP.norm() ? AP.norm() : BP.norm(), false};
    }
  }

  auto ComputeOptimal() -> int;
  template <typename T, typename D0, typename D1> auto QPoint(const T& l0, const T& l1,
                                                              const Eigen::MatrixBase<D0>& P,
                                                              const Eigen::MatrixBase<D1>& R) {
    auto px{P(0)};
    auto py{P(1)};
    auto rx{R(0)};
    auto ry{R(1)};
    using namespace conf::data;
    auto qx{
        (1.0 / 2.0)
        * ((py - ry)
               * (pow(l0, 2) * py - pow(l0, 2) * ry - pow(l1, 2) * py + pow(l1, 2) * ry
                  - pow(px, 2) * py - pow(px, 2) * ry + 2. * px * py * rx + 2. * px * rx * ry
                  - pow(py, 3) + pow(py, 2) * ry - py * pow(rx, 2) + py * pow(ry, 2)
                  - pow(rx, 2) * ry - pow(ry, 3)
                  + sqrt(-(pow(l0, 2) - 2. * l0 * l1 + pow(l1, 2) - pow(px, 2) + 2. * px * rx
                           - pow(py, 2) + 2. * py * ry - pow(rx, 2) - pow(ry, 2))
                         * (pow(l0, 2) + 2. * l0 * l1 + pow(l1, 2) - pow(px, 2) + 2. * px * rx
                            - pow(py, 2) + 2. * py * ry - pow(rx, 2) - pow(ry, 2)))
                        * (px - rx))
           + (-pow(l0, 2) + pow(l1, 2) + pow(px, 2) + pow(py, 2) - pow(rx, 2) - pow(ry, 2))
                 * (pow(px, 2) - 2. * px * rx + pow(py, 2) - 2. * py * ry + pow(rx, 2)
                    + pow(ry, 2)))
        / ((px - rx)
           * (pow(px, 2) - 2. * px * rx + pow(py, 2) - 2. * py * ry + pow(rx, 2) + pow(ry, 2)))};
    auto qy{(1.0 / 2.0)
            * (-pow(l0, 2) * py + pow(l0, 2) * ry + pow(l1, 2) * py - pow(l1, 2) * ry
               + pow(px, 2) * py + pow(px, 2) * ry - 2. * px * py * rx - 2. * px * rx * ry
               + pow(py, 3) - pow(py, 2) * ry + py * pow(rx, 2) - py * pow(ry, 2) + pow(rx, 2) * ry
               + pow(ry, 3)
               + sqrt((-pow(l0, 2) + 2. * l0 * l1 - pow(l1, 2) + pow(px, 2) - 2. * px * rx
                       + pow(py, 2) - 2. * py * ry + pow(rx, 2) + pow(ry, 2))
                      * (pow(l0, 2) + 2. * l0 * l1 + pow(l1, 2) - pow(px, 2) + 2. * px * rx
                         - pow(py, 2) + 2. * py * ry - pow(rx, 2) - pow(ry, 2)))
                     * (-px + rx))
            / (pow(px, 2) - 2. * px * rx + pow(py, 2) - 2. * py * ry + pow(rx, 2) + pow(ry, 2))};
    return D0{qx, qy};
  }
}  // namespace smartbin
