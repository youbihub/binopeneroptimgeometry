# %% poly
import sympy as sp

# compute Q = f(l0,l1,alpha,beta)
# (Q-P).norm()=l0
# (Q-R).norm()=l1

l0, l1, beta = sp.symbols('l0 l1 beta')
px, py, qx, qy, rx, ry = sp.symbols('px py qx qy rx,ry')
theta, phi, y0, y1, t, T = sp.symbols('theta phi y0 y1 t T')

# (qx-px)**2+(qy-py)**2 = l0**2
# (qx-rx)**2+(qy-ry)**2 = l1**2

s = (sp.solve_poly_system([(qx-px)**2+(qy-py)**2-l0**2,
                           (qx-rx)**2+(qy-ry)**2-l1**2], qx, qy))

theta = sp.atan2(qy-py, qx-px)+sp.pi/2
phi = sp.atan2(ry-qy, rx-qx)-(theta-sp.pi/2)

theta.subs([(qx, s[0][0]), (qy, s[0][1])])
phi.subs([(qx, s[0][0]), (qy, s[0][1])])
# %% solve for Ry the height of the bin
h = sp.solve((rx - qx)**2 + (ry-qy)**2-l1**2, ry)

# %% solve theta phi for the control
# P + PQ + QR = R
# P = [px,py] fixed and known
# PQ = l0*[cos(theta-pi/2);sin(theta-pi/2)] theta=0 down
# QR = l1*[cos(theta-pi/2+phi);sin(theta-pi/2+phi)] from previous
# R = [rx,y0+(y1-y0)*(-cos(t/T*pi)+1)/2], rx known, y0,y1<0
P = sp.Matrix([[px], [py]])
PQ = l0*sp.Matrix([[sp.cos(theta-sp.pi/2)], [sp.sin(theta-sp.pi/2)]])
QR = l1*sp.Matrix([[sp.cos(theta-sp.pi/2+phi)], [sp.sin(theta-sp.pi/2+phi)]])
# R = sp.Matrix([[rx], [y0+(y1-y0)*(-sp.cos(t/T*sp.pi)+1)/2]])
R = sp.Matrix([[rx], [ry]])

sys = P+PQ+QR-R
print(sys[0])
sol3 = sp.solve([sys[0], sys[1]], theta, phi)
# %%
