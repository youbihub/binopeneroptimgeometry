# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
from ipywidgets import interactive
from ipywidgets import *
from ipywidgets import interact, interactive, fixed, interact_manual
from matplotlib.widgets import Slider, Button, RadioButtons
import numpy as np
import matplotlib.pyplot as plt
import json

j = json.load(open('measures.json'))
# plot of the sink inside furniture
# data
# x0 = 624
# j['x1'] = 300
# j['x2'] = 153
# j['y0'] = 195
# j['y1'] = 612
# j['y2'] = 245
# j['x3'] = 58
# j['y3'] = 330
# j['x4'] = 305
# j['y4'] = 478
# rp = 17
# rq = 20

# point construction

A = np.array([0, 0])
B = A+[j['x0'], 0]
C = B+[0, -j['y0']]
D = C+[j['x1'], 0]
E = D+[0, -j['y1']]
F = E+[-j['x1']-j['x0']+j['x2'], 0]
G = F+[0, j['y1']+j['y0']-j['y2']]
H = G+[-j['x2'], 0]
I = np.array([j['x2']+j['x3'], -j['y3']])
J = I+[j['x4'], 0]
K = J+[0, -j['y4']]
L = K+[-j['x4'], 0]

dx = np.array([0, j['x0'],   0, j['x1'],   0, -
               j['x1']-j['x0']+j['x2'],        0, -j['x2'],  0])
dy = np.array([0,  0, -j['y0'],  0, -j['y1'],         0,
               j['y1']+j['y0']-j['y2'],   0, j['y2']])
x = np.cumsum(dx)
y = np.cumsum(dy)
fig = plt.figure()
# plt.plot(x, y, 'k')
plt.plot([A[0], B[0], C[0], D[0], E[0], F[0], G[0], H[0], A[0]], [
         A[1], B[1], C[1], D[1], E[1], F[1], G[1], H[1], A[1]], 'k')
ytext = 10

plt.text(j['x0']/2, y[0] + ytext, r'$x_0$')
plt.text((x[2]+x[3])/2, y[2] + ytext, r'$x_1$')
plt.text((x[6]+x[7])/2, y[6] + ytext, r'$x_2$')

plt.text(x[1] + ytext, (y[1]+y[2])/2, r'$y_0$')
plt.text(x[3] + ytext, (y[3]+y[4])/2, r'$y_1$')
plt.text(x[7] + ytext, (y[7]+y[8])/2, r'$y_2$')

# plot of the inside of bin (to move) from beginning to end
dx = np.array([j['x2']+j['x3'], j['x4'],  0, -j['x4'], 0])
dy = np.array([-j['y3'], 0, -j['y4'],  0, j['y4']])
x = np.cumsum(dx)
y = np.cumsum(dy)
# plt.plot(x, y, 'brown')
plt.plot([I[0], J[0], K[0], L[0], I[0]], [
         I[1], J[1], K[1], L[1], I[1]], 'brown')
plt.axes().set_aspect('equal', 'datalim')
plt.text(x[0]+(x[1]-x[0])*0.2, y[0] + ytext, r'$x_4$')
plt.plot([j['x2'], j['x2']+j['x3']],
         [-(2*j['y3']+j['y4'])/2, -(2*j['y3']+j['y4'])/2], '--k')
plt.text(j['x2'], -(2*j['y3']+j['y4'])/2+ytext, r'$x_3$')
plt.text(j['x2']+j['x3']+j['x4']+ytext, -j['y3']-j['y4']*0.8, r'$y_4$')
plt.plot([j['x2']+j['x3']+0.9*j['x4'], j['x2'] +
          j['x3']+0.9*j['x4']], [0, -j['y3']], '--k')
plt.text(j['x2']+j['x3']+0.9*j['x4'], -j['y3']*0.5, r'$y_3$')

plt.text(A[0], A[1], 'A', color='blue', weight='bold')
plt.text(B[0], B[1], 'B', color='blue', weight='bold')
plt.text(C[0], C[1], 'C', color='blue', weight='bold')
plt.text(D[0], D[1], 'D', color='blue', weight='bold')
plt.text(E[0], E[1], 'E', color='blue', weight='bold')
plt.text(F[0], F[1], 'F', color='blue', weight='bold')
plt.text(G[0], G[1], 'G', color='blue', weight='bold')
plt.text(H[0], H[1], 'H', color='blue', weight='bold')


plt.text(I[0], I[1], 'I')
plt.text(J[0], J[1], 'J')
plt.text(K[0], K[1], 'K')
plt.text(L[0], L[1], 'L')

alpha = 100
beta = 80
P = A+[100, -j['rp']]
plt.text(P[0], P[1], 'P')
Q = G+(C-G)*0.78
plt.text(Q[0], Q[1], 'Q')
R = I+(L-I)*0.85+[beta, 0]
plt.text(R[0], R[1], 'R')

plt.plot([P[0], Q[0], R[0]], [P[1], Q[1], R[1]])
plt.plot([I[0]+beta, I[0]+beta], [J[1], K[1]], '--r')
plt.plot([I[0], I[0]+beta], [I[1]+(L[1]-I[1])
                             * 0.15, I[1]+(L[1]-I[1])*0.15], '--b')
plt.text(I[0]+ytext, I[1]+(L[1]-I[1])*0.15, r'$\beta$')
plt.plot([A[0], P[0]], [A[1]-30, A[1] - 30], '--k')
plt.text(A[0]+10, A[1]-30+5, r'$\alpha$')

plt.text((P[0]+Q[0])/2, (P[1]+Q[1])/2, r'$l_0$')
plt.text((Q[0]+R[0])/2, (Q[1]+R[1])/2, r'$l_1$')

theta = np.linspace(0, 2*np.pi)
plt.plot(np.cos(theta)*j['rp']+P[0], np.sin(theta)*j['rp']+P[1])
plt.plot(np.cos(theta)*j['rp']+Q[0], np.sin(theta)*j['rp']+Q[1])

# %% animate
%matplotlib widget

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_aspect('equal', 'datalim')

plt.plot([A[0], B[0], C[0], D[0], E[0], F[0], G[0], H[0], A[0]], [
         A[1], B[1], C[1], D[1], E[1], F[1], G[1], H[1], A[1]], 'k')
plt.plot([I[0], J[0], K[0], L[0], I[0]], [
         I[1], J[1], K[1], L[1], I[1]], 'brown')

# copy this straight from cpp output todo: automate
# l0 = 500
# l1 = 420
# alpha = 100
# beta = 30
# Qmin = [703.038, 84.9525]
# Qmax = [586.459, -357.296]
# Final                            2.030107e+03
l0 = 361.513
l1 = 429.143
alpha = 242.487
beta = 96.2443
Qmin = [604, -20.0001]
Qmax = [272.301, -380.282]
# stop copy
P = A+[alpha, -j['rp']]
PQmin = Qmin-P
PQmax = Qmax-P
theta_min = np.arctan2(PQmin[1], PQmin[0])
theta_max = np.arctan2(PQmax[1], PQmax[0])


# plt.figure()
line0, = ax.plot([0, l0*np.cos(theta_min)], [0, l0*np.sin(theta_min)])
line1, = ax.plot([], [])


def update(w):
    x_0 = P[0]
    y_0 = P[1]
    x_1 = l0*np.cos(w)+x_0
    y_1 = l0*np.sin(w)+y_0
    x_2 = j['x2']+j['x3']+beta
    y_2 = y_1 - np.sqrt((l1 - x_1 + x_2)*(l1 + x_1 - x_2))
    line0.set_xdata([x_0, x_1])
    line0.set_ydata([y_0, y_1])
    line1.set_xdata([x_1, x_2])
    line1.set_ydata([y_1, y_2])
    fig.canvas.draw_idle()
    # plt.show()


interact(update, w=widgets.FloatSlider(min=theta_max,
                                       max=theta_min, step=0.001, value=theta_min))
# interact(update, w=widgets.FloatSlider(min=-np.pi/2,
#                                        max=np.pi/8, step=0.01, value=0))
# %%

# %%
