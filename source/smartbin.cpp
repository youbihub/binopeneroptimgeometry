#include "smartbin/smartbin.h"

#pragma GCC diagnostic push  // disable a  warnings. != for msvc, == for clang
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "ceres/ceres.h"  // for AutoDiffCostFunction
#pragma GCC diagnostic pop

namespace smartbin {

  // Qmin as param so length depends on alpha
  struct LengthFunctor {
    template <typename T> bool operator()(const T* const alpha, const T* const beta,
                                          const T* const q, T* residual) const {
      constexpr auto target{conf::data::y3 + conf::data::y4 - conf::data::rp};

      auto Qmin{Eigen::Map<const Eigen::Matrix<T, 2, 1>>{q}};
      auto P{Eigen::Matrix<T, 2, 1>{alpha[0], -conf::data::rp}};
      auto Rmin{Eigen::Matrix<T, 2, 1>{2}};
      Rmin << (T)conf::point::I(0) + beta[0], (T)conf::point::I(1);

      auto l0{(Qmin - P).norm()};
      auto l1{(Qmin - Rmin).norm()};
      residual[0] = target - (l0 + l1);
      return true;
    }
  };
  struct BetaMidFunctor {
    template <typename T>
    bool operator()(const T* const alpha, const T* const beta, T* residual) const {
      const auto target_alpha{(conf::data::x4) / 2 + conf::point::I(0)};
      constexpr auto target_beta{(conf::data::x4) / 2};
      residual[0] = target_beta - beta[0];
      residual[1] = target_alpha - alpha[0];
      return true;
    }
  };
  // todo: Qminmax directly for dry
  // struct QMinFunctor {
  //   template <typename T> bool operator()(const T* const alpha, const T* const beta,
  //                                         const T* const q, T* residual) const {
  //     auto Qmin{Eigen::Map<const Eigen::Matrix<T, 2, 1>>{q}};
  //     auto P{Eigen::Matrix<T, 2, 1>{alpha[0], -conf::data::rp}};
  //     auto Rmin{Eigen::Matrix<T, 2, 1>{2}};
  //     Rmin << (T)conf::point::I(0) + beta[0], (T)conf::point::I(1) + (T)conf::data::e;

  //     // Avoid BC
  //     // if (Intersect(P, Qm, conf::point::B, conf::point::C)) {
  //     //   return false;  // not needed i think
  //     // }
  //     // if (Qm(1) > -conf::data::rq) {
  //     //   return false;
  //     // }

  //     // auto dist{Distance(P, Qm, conf::point::C)};
  //     // if (dist.d < conf::data::e) {
  //     //   return false;
  //     // }

  //     residual[0] = (T)0;
  //     return true;
  //   }
  // };
  struct QMaxFunctor {
    template <typename T> bool operator()(const T* const alpha, const T* const beta,
                                          const T* const q, T* residual) const {
      auto Qmin{Eigen::Map<const Eigen::Matrix<T, 2, 1>>{q}};
      auto P{Eigen::Matrix<T, 2, 1>{alpha[0], -(T)conf::data::rp}};
      auto l0{(Qmin - P).norm()};
      auto Rmin{Eigen::Matrix<T, 2, 1>{}};
      Rmin << (T)conf::point::I(0) + beta[0], (T)conf::point::I(1);
      auto l1{(Qmin - Rmin).norm()};
      auto Rmax{(conf::point::L + Eigen::Matrix<T, 2, 1>{beta[0], 0.}).eval()};

      // // Avoid IL
      // auto Qm{QPoint(l0, l1, P, Rmax)};
      // if (Intersect(P, Qm, conf::point::I, conf::point::L)
      //     || Intersect(Qm, Rmax, conf::point::I, conf::point::L)) {
      //   return false;
      // }
      // // std::cout << Qm << std::endl;
      auto lmin{(Rmax - P).norm()};
      if (l0 + l1 < lmin) {
        return false;
      }
      // residual[0] = 1. / (Distance(Qm, P, conf::point::G).d);
      residual[0] = (T)0.;
      return true;
    }
  };
  // struct LengthControl {
  //   template <typename T>
  //   bool operator()(const T* const alpha, const T* const q, T* residual) const {
  //     residual[0] = (T)0.;
  //     auto Qmin{Eigen::Map<const Eigen::Matrix<T, 2, 1>>{q}};
  //     auto P{Eigen::Matrix<T, 2, 1>{alpha[0], -conf::data::rp}};
  //     auto l0{(Qmin - P).norm()};
  //     auto lmax{(conf::point::C - P).norm()};
  //     if (l0 >= lmax) {
  //       return false;
  //     }
  //     return true;
  //   }
  // };

  auto ComputeOptimal() -> int {
    // The variable to solve for with its initial value.

    // auto l0{500.};
    // auto l1{420.};
    auto Qmin{Eigen::Vector2d{703.038, 84.9525}};
    auto alpha{100.};
    auto beta{30.};
    // Build the problem.
    ceres::Problem problem;

    // Set up the only cost function (also known as residual). This uses
    // auto-differentiation to obtain the derivative (jacobian).

    problem.AddResidualBlock(
        new ceres::AutoDiffCostFunction<LengthFunctor, 1, 1, 1, 2>(new LengthFunctor), nullptr,
        &alpha, &beta, Qmin.data());
    problem.AddResidualBlock(
        new ceres::AutoDiffCostFunction<BetaMidFunctor, 2, 1, 1>(new BetaMidFunctor), nullptr,
        &alpha, &beta);
    // // problem.AddResidualBlock(
    // //     new ceres::AutoDiffCostFunction<QMinFunctor, 1, 1, 1, 1, 1>(new QMinFunctor), nullptr,
    // //     &l0, &l1, &alpha, &beta);
    problem.AddResidualBlock(
        new ceres::AutoDiffCostFunction<QMaxFunctor, 1, 1, 1, 2>(new QMaxFunctor), nullptr, &alpha,
        &beta, Qmin.data());
    // problem.AddResidualBlock(
    //     new ceres::AutoDiffCostFunction<LengthControl, 1, 1, 2>(new LengthControl), nullptr,
    //     &alpha, Qmin.data());

    // problem.SetParameterLowerBound(&l0, 0, 100);
    // problem.SetParameterUpperBound(&l0, 0, 450);
    // problem.SetParameterLowerBound(&l1, 0, 100);
    // problem.SetParameterUpperBound(&l1, 0, 450);
    // problem.SetParameterLowerBound(&alpha, 0, 20);
    // problem.SetParameterUpperBound(&alpha, 0, 400);
    // problem.SetParameterLowerBound(&beta, 0, 20);
    // problem.SetParameterUpperBound(&beta, 0, 250);
    problem.SetParameterUpperBound(Qmin.data(), 1, -conf::data::rq);
    problem.SetParameterUpperBound(Qmin.data(), 0, conf::data::x0 - conf::data::rq);

    // Run the solver!
    ceres::Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;
    options.max_num_iterations = 500;
    // options.function_tolerance = 1e-12;
    // options.parameter_tolerance = 1e-12;
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);

    std::cout << summary.FullReport() << "\n";
    auto P{Eigen::Vector2d{alpha, -conf::data::rp}};
    auto l0{(Qmin - P).norm()};
    auto Rmin{Eigen::Vector2d{}};
    Rmin << conf::point::I(0) + beta, conf::point::I(1);
    auto l1{(Qmin - Rmin).norm()};
    std::cout << "l0 = " << l0 << "\n";
    std::cout << "l1 = " << l1 << "\n";
    std::cout << "alpha = " << alpha << "\n";
    std::cout << "beta = " << beta << "\n";
    auto Rmax{conf::point::L + Eigen::Vector2d{beta, 0}};
    auto Qmax{QPoint(l0, l1, P, Rmax)};
    std::cout << "Qmin = [" << Qmin(0) << "," << Qmin(1) << "]" << std::endl;
    std::cout << "Qmax = [" << Qmax(0) << "," << Qmax(1) << "]" << std::endl;
    return 0;
  }
}  // namespace smartbin
