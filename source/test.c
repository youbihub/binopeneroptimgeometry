[
  ((1.0 / 2.0)
       * (pow(Iy, 2) - 2 * Iy * e
          - 2 * Iy
                * (-1.0 / 2.0
                       * sqrt(-(pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2
                                - 2 * alpha * x3 + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3
                                - pow(l0, 2) - 2 * l0 * l1 - pow(l1, 2) + pow(x2, 2) + 2 * x2 * x3
                                + pow(x3, 2))
                              * (pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2
                                 - 2 * alpha * x3 + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3
                                 - pow(l0, 2) + 2 * l0 * l1 - pow(l1, 2) + pow(x2, 2) + 2 * x2 * x3
                                 + pow(x3, 2)))
                       * (-alpha + beta + x2 + x3)
                       / (pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2
                          - 2 * alpha * x3 + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3
                          + pow(x2, 2) + 2 * x2 * x3 + pow(x3, 2))
                   - 1.0 / 2.0
                         * (-pow(Iy, 3) + 2 * pow(Iy, 2) * e - Iy * pow(alpha, 2)
                            + 2 * Iy * alpha * beta + 2 * Iy * alpha * x2 + 2 * Iy * alpha * x3
                            - Iy * pow(beta, 2) - 2 * Iy * beta * x2 - 2 * Iy * beta * x3
                            - Iy * pow(l0, 2) + Iy * pow(l1, 2) - Iy * pow(x2, 2) - 2 * Iy * x2 * x3
                            - Iy * pow(x3, 2) + 2 * pow(alpha, 2) * e - 4 * alpha * beta * e
                            - 4 * alpha * e * x2 - 4 * alpha * e * x3 + 2 * pow(beta, 2) * e
                            + 4 * beta * e * x2 + 4 * beta * e * x3 + 2 * e * pow(x2, 2)
                            + 4 * e * x2 * x3 + 2 * e * pow(x3, 2))
                         / (pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2
                            - 2 * alpha * x3 + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3
                            + pow(x2, 2) + 2 * x2 * x3 + pow(x3, 2)))
          - pow(alpha, 2) + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3 + pow(l0, 2) - pow(l1, 2)
          + pow(x2, 2) + 2 * x2 * x3 + pow(x3, 2))
       / (-alpha + beta + x2 + x3),
   -1.0 / 2.0
           * sqrt(-(pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2 - 2 * alpha * x3
                    + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3 - pow(l0, 2) - 2 * l0 * l1
                    - pow(l1, 2) + pow(x2, 2) + 2 * x2 * x3 + pow(x3, 2))
                  * (pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2 - 2 * alpha * x3
                     + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3 - pow(l0, 2) + 2 * l0 * l1
                     - pow(l1, 2) + pow(x2, 2) + 2 * x2 * x3 + pow(x3, 2)))
           * (-alpha + beta + x2 + x3)
           / (pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2 - 2 * alpha * x3
              + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3 + pow(x2, 2) + 2 * x2 * x3
              + pow(x3, 2))
       - 1.0 / 2.0
             * (-pow(Iy, 3) + 2 * pow(Iy, 2) * e - Iy * pow(alpha, 2) + 2 * Iy * alpha * beta
                + 2 * Iy * alpha * x2 + 2 * Iy * alpha * x3 - Iy * pow(beta, 2) - 2 * Iy * beta * x2
                - 2 * Iy * beta * x3 - Iy * pow(l0, 2) + Iy * pow(l1, 2) - Iy * pow(x2, 2)
                - 2 * Iy * x2 * x3 - Iy * pow(x3, 2) + 2 * pow(alpha, 2) * e - 4 * alpha * beta * e
                - 4 * alpha * e * x2 - 4 * alpha * e * x3 + 2 * pow(beta, 2) * e + 4 * beta * e * x2
                + 4 * beta * e * x3 + 2 * e * pow(x2, 2) + 4 * e * x2 * x3 + 2 * e * pow(x3, 2))
             / (pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2 - 2 * alpha * x3
                + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3 + pow(x2, 2) + 2 * x2 * x3
                + pow(x3, 2))),
  ((1.0 / 2.0)
       * (pow(Iy, 2) - 2 * Iy * e
          - 2 * Iy
                * ((1.0 / 2.0)
                       * sqrt(-(pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2
                                - 2 * alpha * x3 + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3
                                - pow(l0, 2) - 2 * l0 * l1 - pow(l1, 2) + pow(x2, 2) + 2 * x2 * x3
                                + pow(x3, 2))
                              * (pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2
                                 - 2 * alpha * x3 + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3
                                 - pow(l0, 2) + 2 * l0 * l1 - pow(l1, 2) + pow(x2, 2) + 2 * x2 * x3
                                 + pow(x3, 2)))
                       * (-alpha + beta + x2 + x3)
                       / (pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2
                          - 2 * alpha * x3 + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3
                          + pow(x2, 2) + 2 * x2 * x3 + pow(x3, 2))
                   - 1.0 / 2.0
                         * (-pow(Iy, 3) + 2 * pow(Iy, 2) * e - Iy * pow(alpha, 2)
                            + 2 * Iy * alpha * beta + 2 * Iy * alpha * x2 + 2 * Iy * alpha * x3
                            - Iy * pow(beta, 2) - 2 * Iy * beta * x2 - 2 * Iy * beta * x3
                            - Iy * pow(l0, 2) + Iy * pow(l1, 2) - Iy * pow(x2, 2) - 2 * Iy * x2 * x3
                            - Iy * pow(x3, 2) + 2 * pow(alpha, 2) * e - 4 * alpha * beta * e
                            - 4 * alpha * e * x2 - 4 * alpha * e * x3 + 2 * pow(beta, 2) * e
                            + 4 * beta * e * x2 + 4 * beta * e * x3 + 2 * e * pow(x2, 2)
                            + 4 * e * x2 * x3 + 2 * e * pow(x3, 2))
                         / (pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2
                            - 2 * alpha * x3 + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3
                            + pow(x2, 2) + 2 * x2 * x3 + pow(x3, 2)))
          - pow(alpha, 2) + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3 + pow(l0, 2) - pow(l1, 2)
          + pow(x2, 2) + 2 * x2 * x3 + pow(x3, 2))
       / (-alpha + beta + x2 + x3),
   (1.0 / 2.0)
           * sqrt(-(pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2 - 2 * alpha * x3
                    + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3 - pow(l0, 2) - 2 * l0 * l1
                    - pow(l1, 2) + pow(x2, 2) + 2 * x2 * x3 + pow(x3, 2))
                  * (pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2 - 2 * alpha * x3
                     + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3 - pow(l0, 2) + 2 * l0 * l1
                     - pow(l1, 2) + pow(x2, 2) + 2 * x2 * x3 + pow(x3, 2)))
           * (-alpha + beta + x2 + x3)
           / (pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2 - 2 * alpha * x3
              + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3 + pow(x2, 2) + 2 * x2 * x3
              + pow(x3, 2))
       - 1.0 / 2.0
             * (-pow(Iy, 3) + 2 * pow(Iy, 2) * e - Iy * pow(alpha, 2) + 2 * Iy * alpha * beta
                + 2 * Iy * alpha * x2 + 2 * Iy * alpha * x3 - Iy * pow(beta, 2) - 2 * Iy * beta * x2
                - 2 * Iy * beta * x3 - Iy * pow(l0, 2) + Iy * pow(l1, 2) - Iy * pow(x2, 2)
                - 2 * Iy * x2 * x3 - Iy * pow(x3, 2) + 2 * pow(alpha, 2) * e - 4 * alpha * beta * e
                - 4 * alpha * e * x2 - 4 * alpha * e * x3 + 2 * pow(beta, 2) * e + 4 * beta * e * x2
                + 4 * beta * e * x3 + 2 * e * pow(x2, 2) + 4 * e * x2 * x3 + 2 * e * pow(x3, 2))
             / (pow(Iy, 2) + pow(alpha, 2) - 2 * alpha * beta - 2 * alpha * x2 - 2 * alpha * x3
                + pow(beta, 2) + 2 * beta * x2 + 2 * beta * x3 + pow(x2, 2) + 2 * x2 * x3
                + pow(x3, 2)))
]