#include "smartbin/smartbin.h"

#include <doctest/doctest.h>

TEST_CASE("Intersection") {
  auto A{Eigen::Vector2d{0, 0}};
  auto B{Eigen::Vector2d{10, 0}};
  auto X{Eigen::Vector2d{2, 5}};
  auto Y{Eigen::Vector2d{2, -12}};
  DOCTEST_CHECK(smartbin::Intersect(A, B, X, Y));
}

TEST_CASE("Optim") { smartbin::ComputeOptimal(); }